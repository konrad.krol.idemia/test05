FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test05.sh"]

COPY test05.sh /usr/bin/test05.sh
COPY target/test05.jar /usr/share/test05/test05.jar
